import 'package:sqflite/sqflite.dart';

class MyDatabase {
  static const TBL_USER = 'TblUser';
  static const USER_ID = 'UserId';
  static const USER_NAME = 'UserName';

  Future<Database> initDatabase() async {
    String databasePath = await getDatabasesPath();
    Database database = await openDatabase(
      '${databasePath}/mydatabase.db',
      version: 1,
      onUpgrade: (db, oldVersion, newVersion) {},
      onCreate: (db, version) {
        db.execute(
          'CREATE TABLE TblUser(UserId INTEGER PRIMARY KEY AUTOINCREMENT,UserName TEXT)',
        );
      },
    );
    return database;
  }

  Future<List<Map<String, dynamic>>> getUserListFromTblUser() async {
    return await (await initDatabase()).rawQuery('SELECT * FROM $TBL_USER');
  }

  Future<int> insertUserDetailInTblUser(String userName) async {
    Database db = await initDatabase();
    Map<String, dynamic> map = {};
    map[USER_NAME] = userName;
    return await db.insert(TBL_USER, map);
  }

  Future<int> updateUserDetailInTblUser(userName, id) async {
    Database db = await initDatabase();
    Map<String, dynamic> map = {};
    map[USER_NAME] = userName;
    return await db
        .update(TBL_USER, map, where: '$USER_ID = ?', whereArgs: [id]);
  }

  Future<int> deleteUserFromTblUser(id) async {
    Database db = await initDatabase();
    return await db.delete(TBL_USER, where: '$USER_ID = ?', whereArgs: [id]);
  }
}
