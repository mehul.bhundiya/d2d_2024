import 'package:flutter/material.dart';

class ScrollableListPage extends StatefulWidget {
  @override
  State<ScrollableListPage> createState() => _ScrollableListPageState();
}

class _ScrollableListPageState extends State<ScrollableListPage> {
  List<Map<String, dynamic>> listValues = [];

  @override
  void initState() {
    super.initState();
    for (int i = 0; i < 50; i++) {
      Map<String, dynamic> map = {};
      map['Name'] = 'Ac Temperature';
      map['Icon'] = Icons.ac_unit;
      map['IsEnabled'] = false;
      listValues.add(map);
      map = {};
      map['Name'] = 'Fan Status';
      map['Icon'] = Icons.mode_fan_off;
      map['IsEnabled'] = false;
      listValues.add(map);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        surfaceTintColor: Colors.transparent,
        backgroundColor: Theme.of(context).primaryColor,
        title: Text(
          'Scrollabe List',
          style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(10),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Text('Products', style: TextStyle(color: Colors.red)),
          Container(
            height: 120,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return SizedBox(
                  width: 80,
                  height: 100,
                  child: Card(
                    child: Column(children: [
                      Expanded(
                          child: ClipRRect(
                        child: Image.asset(
                          'assets/images/bg_1.jpg',
                          fit: BoxFit.cover,
                        ),
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(12),
                          topRight: Radius.circular(12),
                        ),
                      )),
                      Text(
                        'Machienary',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ]),
                  ),
                );
              },
              itemCount: 10,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text('Catrgories', style: TextStyle(color: Colors.red)),
          Container(
            height: 120,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return SizedBox(
                  width: 80,
                  height: 100,
                  child: Card(
                    child: Column(children: [
                      Expanded(child: Image.asset('assets/images/bg_1.jpg')),
                      Text(
                        'Machienary',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ]),
                  ),
                );
              },
              itemCount: 10,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text('Latest Videos', style: TextStyle(color: Colors.red)),
          Container(
            height: 120,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return SizedBox(
                  width: 80,
                  height: 100,
                  child: Card(
                    child: Column(children: [
                      Expanded(child: Image.asset('assets/images/bg_1.jpg')),
                      Text(
                        'Machienary',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ]),
                  ),
                );
              },
              itemCount: 10,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text('TV Shows', style: TextStyle(color: Colors.red)),
          Container(
            height: 120,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return SizedBox(
                  width: 80,
                  height: 100,
                  child: Card(
                    child: Column(children: [
                      Expanded(child: Image.asset('assets/images/bg_1.jpg')),
                      Text(
                        'Machienary',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ]),
                  ),
                );
              },
              itemCount: 10,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Text('Popular Shows', style: TextStyle(color: Colors.red)),
          Container(
            height: 120,
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return SizedBox(
                  width: 80,
                  height: 100,
                  child: Card(
                    child: Column(children: [
                      Expanded(child: Image.asset('assets/images/bg_1.jpg')),
                      Text(
                        'Machienary',
                        style: TextStyle(
                          fontSize: 12,
                        ),
                      ),
                    ]),
                  ),
                );
              },
              itemCount: 10,
            ),
          ),
        ]),
      ),
    );
  }

  List<Widget> getColumnWidgets() {
    List<Widget> widgets = [];
    for (int i = 0; i < 100; i++) {
      widgets.add(
        getCustomListTile(
          title: listValues[1]['Name'],
          isEnabled: listValues[1]['IsEnabled'],
          leadingIcon: listValues[1]['Icon'],
          onSwitchChangeValue: (value) {
            setState(() {
              listValues[1]['IsEnabled'] = !listValues[1]['IsEnabled'];
            });
          },
        ),
      );
    }
    return widgets;
  }

  Widget getCustomListTile(
      {String title = '',
      IconData? leadingIcon,
      Color? leadingIconColor,
      IconData? trailingIcon = Icons.keyboard_arrow_right,
      Color? trailingIconColor,
      bool isEnabled = false,
      onSwitchChangeValue}) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: ListTile(
        title: Text(
          title,
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        leading: Icon(
          leadingIcon,
          color: isEnabled ? Colors.green : Colors.red,
        ),
        trailing: Switch(
          value: isEnabled,
          onChanged: (value) {
            onSwitchChangeValue(value);
          },
        ),
        // trailing: Icon(
        //   trailingIcon,
        //   color: trailingIconColor ?? Colors.grey.shade400,
        // ),
      ),
    );
  }
}
