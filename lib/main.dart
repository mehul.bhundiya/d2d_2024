import 'package:flutter/material.dart';
import 'package:my_d2d_project/boost_post.dart';
import 'package:my_d2d_project/bottom_nav_page.dart';
import 'package:my_d2d_project/db/my_database.dart';
import 'package:my_d2d_project/pre_login_page.dart';
import 'package:my_d2d_project/row_column.dart';
import 'package:my_d2d_project/screen1.dart';
import 'package:my_d2d_project/scrollabel_list.dart';
import 'package:my_d2d_project/user_list_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          useMaterial3: true,
          primaryColor: Colors.cyan),
      home: FutureBuilder(
        future: MyDatabase().initDatabase(),
        builder: (context, snapshot) {
          if (snapshot.hasData && snapshot.data != null) {
            return UserListPage();
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  List<String> dataList = [
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
    'Mehul',
    'Raj',
    'Jayesh',
    'Rupesh',
    'Firoz',
    'Pradhyuman',
    'Maulik',
  ];

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  bool isGridView = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(children: [
              InkWell(
                child: Icon(Icons.list),
                onTap: () {
                  setState(() {
                    isGridView = false;
                  });
                },
              ),
              InkWell(
                child: Icon(Icons.grid_3x3),
                onTap: () {
                  setState(() {
                    isGridView = true;
                  });
                },
              ),
            ]),
          ),
          Expanded(
            child: isGridView ? getGridWidget() : getListWidget(),
          ),
        ],
      ),
    );
  }

  getListWidget() {
    return ListView.builder(
      itemBuilder: (context, index) {
        return Card(
          elevation: 10,
          margin: EdgeInsets.fromLTRB(
              10, 10, 10, (index == dataList.length - 1) ? 10 : 0),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
            child: Text(
              dataList[index],
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
      },
      itemCount: dataList.length,
    );
  }

  getGridWidget() {
    return GridView.builder(
      itemBuilder: (context, index) {
        return Card(
          elevation: 10,
          margin: EdgeInsets.fromLTRB(
              10, 10, 10, (index == dataList.length - 1) ? 10 : 0),
          child: Padding(
            padding: const EdgeInsets.fromLTRB(20, 8, 20, 8),
            child: Text(
              dataList[index],
              style: TextStyle(
                fontSize: 20,
                color: Colors.black,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        );
      },
      itemCount: dataList.length,
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
    );
  }
}
