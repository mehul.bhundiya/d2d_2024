import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_d2d_project/api/api_executor.dart';
import 'package:my_d2d_project/burger.dart';
import 'package:my_d2d_project/db/my_database.dart';
import 'package:my_d2d_project/user_entry_page.dart';

class UserListPage extends StatefulWidget {
  @override
  State<UserListPage> createState() => _UserListPageState();
}

class _UserListPageState extends State<UserListPage> {
  List<dynamic> userNameList = [];

  List<dynamic> filterList = [];

  ApiExecutor api = ApiExecutor();

  MyDatabase db = MyDatabase();

  @override
  void initState() {
    super.initState();
    filterList.addAll(userNameList);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('User List'),
        actions: [
          IconButton(
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) {
                  return UserEntryPage();
                },
              )).then((value) {
                print('VALUE RETURNED :1:: $value');
                if (value != null) {
                  print('VALUE RETURNED :2:: $value');
                  setState(() {
                    userNameList.clear();
                    filterList.clear();
                  });
                }
              });
            },
            icon: Icon(Icons.add),
          ),
        ],
      ),
      body: Column(
        children: [
          SearchBar(
            leading: Icon(Icons.search),
            onChanged: (value) {
              filterList.clear();
              if (value.isNotEmpty) {
                for (int i = 0; i < userNameList.length; i++) {
                  if (userNameList[i]
                      .toString()
                      .toLowerCase()
                      .contains(value.toLowerCase())) {
                    filterList.add(userNameList[i]);
                  }
                }
              } else {
                filterList.addAll(userNameList);
              }
              setState(() {});
            },
          ),
          Expanded(
            child: FutureBuilder(
              future: api.getUserListFromUserListAPI(),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data != null) {
                  userNameList.clear();
                  filterList.clear();
                  userNameList.addAll(snapshot.data!);
                  filterList.addAll(userNameList);
                  if (filterList.isNotEmpty) {
                    return ListView.builder(
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(
                              builder: (context) {
                                return UserEntryPage(
                                  userObject: filterList[index],
                                );
                              },
                            )).then((value) {
                              if (value != null) setState(() {});
                            });
                          },
                          child: Card(
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  Image.network(
                                    filterList[index]['imageUrl'].toString(),
                                    fit: BoxFit.cover,
                                    height: 100,
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        child: Text(
                                          filterList[index]['title'].toString(),
                                          style: TextStyle(fontSize: 25),
                                        ),
                                      ),
                                      IconButton(
                                        onPressed: () {
                                          showAlertDialog(index);
                                        },
                                        icon: Icon(
                                          Icons.delete,
                                          color: Colors.red,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                      itemCount: filterList.length,
                    );
                  } else {
                    return Center(
                      child: Text('No Category Found'),
                    );
                  }
                } else {
                  return Center(
                    child: Container(
                      width: 100,
                      height: 100,
                      child: CupertinoActivityIndicator(
                        radius: 10.0,
                        animating: true,
                      ),
                    ),
                  );
                }
              },
            ),
          ),
        ],
      ),
    );
  }

  void showAlertDialog(index) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('Alert!'),
          content: Text('Are you sure want to delete?'),
          actions: [
            TextButton(
              onPressed: () async {
                // await db.deleteUserFromTblUser(
                //     filterList[index][MyDatabase.USER_ID]);
                api.showProgressDialog(context);
                await api.deleteCategoryUsingDelete(
                    userNameList[index][ApiExecutor.ID]);
                api.dismissProgressDialog();
                setState(() {});
                Navigator.pop(context);
              },
              child: Text('Yes'),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('No'),
            )
          ],
        );
      },
    );
  }
}
