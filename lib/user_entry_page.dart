import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_d2d_project/api/api_executor.dart';
import 'package:my_d2d_project/db/my_database.dart';

class UserEntryPage extends StatelessWidget {
  Map<dynamic, dynamic>? userObject;

  UserEntryPage({this.userObject}) {
    titleController.text =
        userObject != null ? userObject![ApiExecutor.TITLE] : '';
    imageUrlController.text =
        userObject != null ? userObject![ApiExecutor.IMAGE_URL] : '';
  }

  TextEditingController titleController = TextEditingController();
  TextEditingController imageUrlController = TextEditingController();

  MyDatabase db = MyDatabase();
  ApiExecutor apiExecutor = ApiExecutor();
  GlobalKey<FormState> _formKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        title: Text('${userObject != null ? 'Edit' : 'Add'} User'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _formKey,
          child: Column(
            children: [
              TextFormField(
                controller: titleController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Title';
                  }
                },
                decoration: InputDecoration(
                  hintText: 'Enter Title',
                  labelText: 'Enter Title',
                ),
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                controller: imageUrlController,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Image URL';
                  }
                },
                decoration: InputDecoration(
                  hintText: 'Enter Image URL',
                  labelText: 'Enter Image URL',
                ),
              ),
              SizedBox(
                height: 20,
              ),
              ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      int userId = 0;
                      if (userObject == null) {
                        Map<String, dynamic> map = {};
                        map[ApiExecutor.IMAGE_URL] =
                            imageUrlController.text.toString();
                        map[ApiExecutor.TITLE] =
                            titleController.text.toString();
                        apiExecutor.showProgressDialog(context);
                        Map<dynamic, dynamic> userMap =
                            await apiExecutor.insertCategoryInPOST(map);
                        apiExecutor.dismissProgressDialog();
                        // userId = await db.insertUserDetailInTblUser(
                        //     titleController.text.toString());
                        if (userMap != null) {
                          Navigator.of(context).pop(true);
                        }
                      } else {
                        userObject![ApiExecutor.IMAGE_URL] =
                            imageUrlController.text.toString();
                        userObject![ApiExecutor.TITLE] =
                            titleController.text.toString();
                        Map<dynamic, dynamic> userMap =
                            await apiExecutor.updateCategoryInPUT(userObject!);
                        if (userMap != null) {
                          Navigator.of(context).pop(true);
                        }
                        // userId = await db.updateUserDetailInTblUser(
                        //   titleController.text.toString(),
                        //   userObject![MyDatabase.USER_ID],
                        // );
                      }
                      // if (userId > 0) {
                      //   showDatabaseAlert(context, 'Success');
                      // } else {
                      //   showDatabaseAlert(context, 'Failure');
                      // }
                    }
                  },
                  child: Text('Submit'))
            ],
          ),
        ),
      ),
    );
  }

  showDatabaseAlert(context, message) {
    showCupertinoDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text(message.toString()),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
                Navigator.of(context).pop(true);
              },
              child: Text('ok'),
            ),
          ],
        );
      },
    );
  }
}
