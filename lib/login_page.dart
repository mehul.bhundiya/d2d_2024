import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class LoginPage extends StatefulWidget {
  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool isChecked = true;

  GlobalKey<FormState> _globalKey = GlobalKey<FormState>();

  String filterVariable = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Form(
          key: _globalKey,
          child: Column(
            children: [
              TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30)),
                  hintText: 'Enter UserName',
                  labelText: 'UserName',
                ),
                keyboardType: TextInputType.number,
                inputFormatters: <TextInputFormatter>[
                  FilteringTextInputFormatter.digitsOnly
                ],
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter UserName';
                  }
                  // if (!RegExp(
                  //         r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                  //     .hasMatch(value.toString())) {
                  //   return 'Enter Valid Username';
                  // }
                  return null;
                },
              ),
              SizedBox(
                height: 10,
              ),
              TextFormField(
                decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(30)),
                  hintText: 'Enter Password',
                  labelText: 'Password',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Enter Password';
                  }
                  if (value.length < 6) {
                    return 'Enter Valid Password';
                  }
                  return null;
                },
              ),
              GestureDetector(
                onTap: () {
                  setState(() {
                    isChecked = !isChecked;
                  });
                },
                child: Row(
                  children: [
                    Checkbox(
                      value: isChecked,
                      onChanged: (value) {},
                    ),
                    Text("Remember me"),
                  ],
                ),
              ),
              TextButton(
                onPressed: () {
                  // if (_globalKey.currentState!.validate()) {
                  showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (context) {
                      return StatefulBuilder(
                        builder: (context, setState) {
                          return Row(
                            children: [
                              Expanded(
                                child: ListView.builder(
                                  itemBuilder: (context, index) {
                                    return ListTile(
                                      onTap: () {
                                        setState(() {
                                          filterVariable = 'Data $index';
                                        });
                                      },
                                      title: Text('Data $index'),
                                    );
                                  },
                                  itemCount: 20,
                                ),
                                flex: 2,
                              ),
                              VerticalDivider(
                                color: Colors.grey.shade400,
                                width: 1,
                              ),
                              Expanded(
                                child: Column(
                                  children: [
                                    TextFormField(),
                                    Text(filterVariable),
                                  ],
                                ),
                                flex: 4,
                              ),
                            ],
                          );
                        },
                      );
                    },
                  );
                  // }
                },
                child: Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
