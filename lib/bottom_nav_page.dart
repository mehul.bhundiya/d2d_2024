import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_d2d_project/boost_post.dart';
import 'package:my_d2d_project/pre_login_page.dart';
import 'package:my_d2d_project/screen1.dart';
import 'package:my_d2d_project/scrollabel_list.dart';

class BottomNavPage extends StatefulWidget {
  @override
  State<BottomNavPage> createState() => _BottomNavPageState();
}

class _BottomNavPageState extends State<BottomNavPage> {
  int bottomNavIndex = 0;

  List<Widget> screens = [
    BoostPostRoute(),
    PreLoginPage(),
    ScrollableListPage(),
    Screen1Route(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('DRAWER'),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: screens[bottomNavIndex],
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: FloatingActionButton(
          onPressed: () {},
          shape: CircleBorder(),
          child: const Icon(Icons.home_filled),
        ),
        drawer: Drawer(
          child: Column(
            children: [
              Container(
                color: Theme.of(context).primaryColor,
                child: Row(
                  children: [
                    DrawerHeader(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                        ),
                        child: const Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              'Hi, Mehul Bhundiya',
                              style: TextStyle(
                                color: Colors.white,
                              ),
                            ),
                            Text(
                              'mehul.bhundiya@darshan.ac.in',
                              style: TextStyle(
                                fontSize: 12,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
              getDrawerMenuWidget('Home', Icons.home),
              Divider(
                color: Colors.grey.shade300,
              ),
              getDrawerMenuWidget('List', Icons.list),
              Divider(
                color: Colors.grey.shade300,
              ),
              getDrawerMenuWidget('Alert', Icons.add_alert,
                  onListTileClick: () {
                Navigator.pop(context);
                showCupertinoDialog(
                  context: context,
                  builder: (context) {
                    return CupertinoAlertDialog(
                      title: Text('ALERT!'),
                      content: Text('ALERT MESSAGE FROM DRAWER'),
                      actions: [
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('Yes'),
                        ),
                        TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text('No'),
                        ),
                      ],
                    );
                  },
                );
              }),
              Divider(
                color: Colors.grey.shade300,
              ),
              Expanded(child: Container()),
              getDrawerMenuWidget('Logout', Icons.logout,
                  onListTileClick: () {
                Navigator.of(context).pop();
              }),
            ],
          ),
        ),
        bottomNavigationBar: BottomAppBar(
            color: Colors.transparent,
            shape: const CircularNotchedRectangle(),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.shopping_bag),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.money),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.currency_bitcoin),
                ),
                IconButton(
                  onPressed: () {},
                  icon: const Icon(Icons.bar_chart),
                ),
              ],
            )));
  }

  Widget getDrawerMenuWidget(title, icon, {onListTileClick}) {
    return ListTile(
      onTap: onListTileClick,
      title: Text(title),
      leading: Icon(icon),
      trailing: Icon(
        Icons.arrow_forward_ios,
        size: 15,
        color: Colors.grey.shade500,
      ),
    );
  }
}
