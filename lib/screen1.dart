import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:my_d2d_project/user_list_page.dart';

import 'burger.dart';

class Screen1Route extends StatefulWidget {
  @override
  State<Screen1Route> createState() => _Screen1RouteState();
}

class _Screen1RouteState extends State<Screen1Route> {
  Future<int> callOrder() async {
    print(
        'GET BURGER ORDER ::: ${getBurgerWithoutFuture('McAloo', 2)}');
    print(
        'GET BURGER ORDER :FUTURE:: ${await getBurger('McAloo', 2)}');
    print('ORDERS FINISHED');
    return 1;
  }

  Burger getBurgerWithoutFuture(String burgerName, int qty) {
    return Burger(burgerName: burgerName, qty: qty);
  }

  Future<Burger> getBurger(String burgerName, int qty) {
    return Future.delayed(
      Duration(seconds: 3),
          () {
        return Burger(burgerName: burgerName, qty: qty);
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    callOrder();
    return Scaffold(
      body: Stack(
        children: [
          Image.asset(
            'assets/images/bg_1.jpg',
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.cover,
          ),
          Container(color: Colors.black.withOpacity(0.2)),
          Center(
            child: Text(
              'Happy Birthday',
              style: GoogleFonts.aboreto(
                  textStyle: TextStyle(
                color: Colors.white,
                fontSize: 25,
              )),
            ),
          ),
        ],
      ),
    );
  }
}
