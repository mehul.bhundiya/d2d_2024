import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';

class ApiExecutor {
  String API_URL = 'https://638029948efcfcedacfe0228.mockapi.io/api/Categories';

  static const String TITLE = 'title';
  static const String IMAGE_URL = 'imageUrl';
  static const String ID = 'id';
  ProgressDialog? progressDialog;

  void showProgressDialog(context, {msg}) {
    progressDialog = ProgressDialog(
      context,
      isDismissible: false,
    );
    progressDialog!
        .style(message: msg ?? 'Please wait while data is under process');
    progressDialog!.show();
  }

  void dismissProgressDialog() {
    if (progressDialog != null) progressDialog!.hide();
  }

  Future<List<dynamic>> getUserListFromUserListAPI() async {
    http.Response response = await http.get(Uri.parse(API_URL));
    List<dynamic> userList = jsonDecode(response.body);
    return userList;
  }

  Future<Map<dynamic, dynamic>> insertCategoryInPOST(
      Map<String, dynamic> map) async {
    http.Response response = await http.post(Uri.parse(API_URL), body: map);
    Map<dynamic, dynamic> categoryObject = jsonDecode(response.body);
    return categoryObject;
  }

  Future<Map<dynamic, dynamic>> updateCategoryInPUT(
      Map<dynamic, dynamic> map) async {
    http.Response response =
        await http.put(Uri.parse(API_URL + '/' + map['id']), body: map);
    Map<dynamic, dynamic> categoryObject = jsonDecode(response.body);
    return categoryObject;
  }

  Future<Map<dynamic, dynamic>> deleteCategoryUsingDelete(id) async {
    http.Response response = await http.delete(Uri.parse(API_URL + '/' + id));
    Map<dynamic, dynamic> categoryObject = jsonDecode(response.body);
    return categoryObject;
  }
}
