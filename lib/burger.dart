class Burger {
  int? _qty;
  String? _burgerName;

  Burger({required qty,required burgerName}) {
    this._qty = qty;
    this._burgerName = burgerName;
  }

  int get qty => _qty ?? 0;

  set qty(int value) {
    _qty = value;
  }

  String get burgerName => _burgerName ?? '';

  set burgerName(String value) {
    _burgerName = value;
  }
}
